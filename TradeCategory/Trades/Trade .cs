﻿using System;

namespace TradeCategory
{
    public class Trade : ITrade
    {
        private double _value;
        private DateTime _nextPaymentDate, _dateReference;
        private string _clientSector;
        private bool _isPoliticallyExposed;

        // Construtor       
        public Trade(double value, DateTime nextPaymentDate, DateTime dateReference, string clientSector, bool isPoliticallyExposed)
        {
            _value = value;
            _nextPaymentDate = nextPaymentDate;
            _dateReference = dateReference;
            _clientSector = clientSector;
            _isPoliticallyExposed = isPoliticallyExposed;
        }

        // Imp. Obj. da interface
        public double Value
        {
            get
            {
                return _value;
            }
        }
        public DateTime NextPaymentDate
        {
            get
            {
                return _nextPaymentDate;
            }
        }
        public DateTime DateReference
        {
            get
            {
                return _dateReference;
            }
        }
        public string ClientSector
        {
            get
            {
                return _clientSector;
            }
        }
        public bool IsPoliticallyExposed
        {
            get
            {
                return _isPoliticallyExposed;
            }
        }

    }

}

