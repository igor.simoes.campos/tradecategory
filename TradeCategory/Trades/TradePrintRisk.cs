﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeCategory
{
    public static class TradePrintRisk
    {
        private static ExpiredCategory expired = new ExpiredCategory();
        private static HighRiskCategory high = new HighRiskCategory();
        private static MediumRiskCategory medium = new MediumRiskCategory();
        private static IsPoliticallyExposedCategory isPoliticallyExposed = new IsPoliticallyExposedCategory(); // Classificacao nova da ITrade
        private static NotFound notFound = new NotFound(); // Caso nao exista classificacao

        public static void PrintClassificationRisk(List<Trade> portifolios)
        {
            // loop dados da portifolio
            foreach (var p in portifolios)
            {
                // Cria lista de classificacao de risco
                List<IClassification> riskClassification = new List<IClassification> { expired, high, medium, isPoliticallyExposed, notFound };

                // Imprime a classificacao de risco do portifolio do trade
                foreach (var r in riskClassification)                
                    if (r.Applies(p))
                    {
                        Console.Write(r.Name() + "\n");
                        break;
                    }
                
            }

        }
    }
}
