﻿using System;
using System.Collections.Generic;

namespace TradeCategory
{
    class Program
    {
        static void Main(string[] args)
        {   
            // Inicializa os valores dos objetos fixos
            string numInput1 = "12/11/2020";
            string numInput2 = "4";

            // Cria portifolios do trade
            Trade tPortfolio1 = new Trade(value: 2000000, nextPaymentDate: DateTime.ParseExact("12/29/2025", "MM/dd/yyyy", null), dateReference: DateTime.ParseExact(numInput1, "MM/dd/yyyy", null), clientSector: "Private", isPoliticallyExposed: false);
            Trade tPortfolio2 = new Trade(value: 400000, nextPaymentDate: DateTime.ParseExact("07/01/2020", "MM/dd/yyyy", null), dateReference: DateTime.ParseExact(numInput1, "MM/dd/yyyy", null), clientSector: "Public", isPoliticallyExposed: false);
            Trade tPortfolio3 = new Trade(value: 5000000, nextPaymentDate: DateTime.ParseExact("01/02/2024", "MM/dd/yyyy", null), dateReference: DateTime.ParseExact(numInput1, "MM/dd/yyyy", null), clientSector: "Public", isPoliticallyExposed: true);
            Trade tPortfolio4 = new Trade(value: 3000000, nextPaymentDate: DateTime.ParseExact("10/26/2023", "MM/dd/yyyy", null), dateReference: DateTime.ParseExact(numInput1, "MM/dd/yyyy", null), clientSector: "Public", isPoliticallyExposed: false);

            // Cria a lista de trades
            List<Trade> portifolios = new List<Trade> { tPortfolio1, tPortfolio2, tPortfolio3, tPortfolio4 };

            // Imprime dados do portifolio 
            Console.WriteLine("---------------------------------------\n");
            Console.WriteLine("Categorize trades in a bank´s portfolio\r");
            Console.WriteLine("---------------------------------------\n");
            Console.WriteLine("Sample Input:\n");           
            Console.WriteLine(numInput1);            
            Console.WriteLine(numInput2); 
            foreach (var item in portifolios)            
                Console.WriteLine(item.Value.ToString() + " " + item.ClientSector + " " + item.NextPaymentDate.ToString("MM/dd/yyyy"));
                        
            Console.WriteLine("\n");
            Console.WriteLine("Sample Output:\n");

            // Imprime dos portifolios exibindo a sua classificacao de risco
            TradePrintRisk.PrintClassificationRisk(portifolios);
            
            Console.WriteLine("\n");
            Console.Write("Press Enter to exit: ");
            string r = Console.ReadLine();

            return;
        }
                
    }

}
