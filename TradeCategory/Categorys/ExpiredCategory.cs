﻿using System;

namespace TradeCategory
{
    public class ExpiredCategory : IClassification
    {
        public string Name()
        {
            return Category.EXPIRED;
        }

        public bool Applies(Trade trade)
        {
            TimeSpan diff = trade.NextPaymentDate - trade.DateReference;
            if (diff.Days < 30)
                return true;
            else
                return false;
        }
    }
}