﻿namespace TradeCategory
{
    class NotFound : IClassification
    {
        public string Name()
        {
            return Category.NOTFOUND;
        }

        public bool Applies(Trade trade)
        {
            return true;
        }
    }
}