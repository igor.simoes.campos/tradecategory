﻿namespace TradeCategory
{
    public class IsPoliticallyExposedCategory : IClassification
    {
        public string Name()
        {
            return Category.ISPOLITICALLYEXPOSED;
        }

        public bool Applies(Trade trade)
        {
            if (trade.IsPoliticallyExposed)
                return true;
            else
                return false;

        }

    }
}