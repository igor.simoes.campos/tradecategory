﻿namespace TradeCategory
{
    public class MediumRiskCategory : IClassification
    {
        public string Name()
        {
            return Category.MEDIUMRISK;
        }

        public bool Applies(Trade trade)
        {
            if (trade.Value < 1000000 && trade.ClientSector == Sectors.PUBLIC)
                return true;
            else
                return false;
        }
    }
}