﻿namespace TradeCategory
{
    public class HighRiskCategory : IClassification
    {
        public string Name()
        {
            return Category.HIGHRISK;
        }

        public bool Applies(Trade trade)
        {
            if (trade.Value >= 1000000 && trade.ClientSector == Sectors.PRIVATE)
                return true;
            else
                return false;
        }
    }
}