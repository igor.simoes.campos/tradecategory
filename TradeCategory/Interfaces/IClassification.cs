﻿namespace TradeCategory
{
    public interface IClassification
    {
        bool Applies(Trade trade);
        string Name();
    }
}