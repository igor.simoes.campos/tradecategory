﻿namespace TradeCategory
{
    public static class Category
    {
        public static string EXPIRED = "EXPIRED";
        public static string HIGHRISK = "HIGHRISK";
        public static string MEDIUMRISK = "MEDIUMRISK";
        public static string ISPOLITICALLYEXPOSED = "IsPoliticallyExposed";
        public static string NOTFOUND = "NOT FOUND";
    }
}